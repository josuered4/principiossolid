class Figura:
    def calcular_area(self):
        pass

class Rectangulo(Figura):
    def __init__(self, ancho, alto):
        self.ancho = ancho
        self.alto = alto
    
    def calcular_area(self):
        return self.ancho * self.alto
    
    def obtener_lado_mas_largo(self):
        if self.ancho > self.alto:
            return self.ancho
        else:
            return self.alto
    
        
      
if __name__ == "__main__":
    figura = Rectangulo(10, 5)
    area = figura.calcular_area()
    
