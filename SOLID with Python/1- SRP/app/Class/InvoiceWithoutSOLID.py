
class InvoiceWithoutSOLID:

    def __init__(self, book, amount, discount_rate, tax_rate):
        self.book = book
        self.amount = amount
        self.discount_rate = discount_rate
        self.tax_rate = tax_rate
        self.total = self.calculate_total()
        
    def calculate_total(self):
        price = ((self.book.price - self.book.price * self.discount_rate) * self.amount)
        priceWithTax = price * (1 + self.tax_rate)
        return priceWithTax

    def print_invoice(self):
        print(self.amount + "x " + self.book.name + " " + self.book.price+"$")
        print("Tasa de descuento: " + self.discount_rate + "\nTasa de Impuestos: " + self.tax_rate + "\nTotal: "+self.total)
        
    def save_file(self, name_file):
        print("Se guardo en " + name_file)
        
# Las clase anterior viola el principio de responsabilidad unica 
# pero por que:
#     Esta clase tiene mas de una responsabilidad
#         1-instanciar los objetos factura 
#         2-imprimir la factura 
#         3-guardar en un archivo 
#         4-calcular el total

# Solucion.
#     Separar las funciones en clases diferentes, con exepcion de calcular_total 