
class InvoiceWithSOLID:

    def __init__(self, book, amount, discount_rate, tax_rate):
        self.book = book
        self.amount = amount
        self.discount_rate = discount_rate
        self.tax_rate = tax_rate
        self.total = self.calculate_total()
        
    def calculate_total(self):
        price = ((self.book.price - self.book.price * self.discount_rate) * self.amount)
        priceWithTax = price * (1 + self.tax_rate)
        return priceWithTax
        
        
class PrintInvouce:
    
    def __init__(self, invoiceWithSOLID):
        self.invoiceWithSOLID = invoiceWithSOLID
    
    def print_invoice(self):
        print(self.invoiceWithSOLID.amount + "x " + self.invoiceWithSOLID.book.name + " " + self.invoiceWithSOLID.book.price+"$")
        print("Tasa de descuento: " + self.discount_rate + "\nTasa de Impuestos: " + self.tax_rate + "\nTotal: "+self.total)
    

class SaveInvouce:
    
    def __init__(self, invoiceWithSOLID):
        self.invoiceWithSOLID = invoiceWithSOLID
    
    def save_file(self, name_file):
        print("Se guardo en " + name_file)
        
# Implementacion de S de Solid 
# Separamos las funciones en clases diferentes,
# dado a que el principio de responsabilidad unica 
# dice que solo una responabilidad por clase. 

# pero que pasa en el caso de la funcion de calculate_total,
# es un caso especial, se podria separar y es valido, pero 
# esta funcion esta relacionada a la clase, dado a que ambas 
# funciones pueden cambiar por la misma razon

#Ahora cuando queramos editar una funcionalidad nos vamos a una sola clase