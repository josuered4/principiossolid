from Class.InvoiceWithoutSOLID import InvoiceWithoutSOLID
from Class.InvoiceWithSOLID import InvoiceWithSOLID, SaveInvouce
from Models.Models import Book

if __name__ == '__main__':
    print("Hola Mundo")
    book = Book("Example", "josue reyes", 2000, 10, "isbn")
    var = InvoiceWithoutSOLID(book, 5, 20, 10)
    print(var.calculate_total())
    
    book = Book("Example2", "josue reyes", 2000, 10, "isbn")
    var = InvoiceWithSOLID(book, 5, 20, 10)
    save = SaveInvouce(var)
    save.save_file("name_file")

#Ejemplo de una mala practica
# De una clase libro tenemos que crear facturas por libro y 
# hacer guardado en BBDD
