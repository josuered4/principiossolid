Principio de Abierto/Cerrado

Las clases deban estar abiertas a la extensión y cerradas a la modificación.

Modificación significa cambiar el código de una clase existente y extensión 
significa agregar una nueva funcionalidad.

Reto:
Deberíamos poder agregar nuevas funciones sin tocar el código existente para la clase.
sto se debe a que cada vez que modificamos el código existente, corremos el riesgo de 
crear errores potenciales.

Cuestion y Resultados:
Pero, ¿cómo vamos a agregar una nueva funcionalidad sin tocar la clase?, 
puede preguntarse. Por lo general, se hace con la ayuda de interfaces y clases abstractas.
