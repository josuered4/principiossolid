from abc import ABC, abstractmethod

#Creamos una interfaz SaveInvoice para definir 
#un tipo de clases o funciones 
class SaveInvoice:
    @abstractmethod
    def save(self, InvoiceWithSOLID):
        pass

#Ahora SaveFile es de tipo  SaveInvoice
class SaveFile(SaveInvoice):
    def save(self, InvoiceWithSOLID):
        print("Se guardo en un archivo")
        
#Ahora SaveDataBase es de tipo  SaveInvoice       
class SaveDataBase(SaveInvoice):
    
    def save(self, InvoiceWithSOLID):
        print("Se guardo en Base de Datos")
    
    
        
#Ahora la clase SaveInvouceWithSolid trabajara con una 
#intancia de cualquier clase que implemente saveInvoice
class SaveInvouceWithSolid:
    def __init__(self, invoiceWithSOLID, saveInvoice):
        self.invoiceWithSOLID = invoiceWithSOLID
        self.saveInvoice = saveInvoice
        
    def save(self):
        self.saveInvoice.save(self.invoiceWithSOLID)
        
        
# Ahora bien si queremos agregar mas metodos de guardado 
# deberemos crear clase que implementen saveInvoice, y 
# pasarlos al SaveInvouceWithSolid, dado que es la clase 
# principal, ademas de que de esta manera ya no modificaremos esta

#Este principio complementa un poco al anterior dado a que si 
#agregabamos mas metodos a la clase, ya no tendria una sola responsabilidad
#dado a que guardar en files o ddbb son cosas diferentes 
    
    
