class Book:
    def __init__(self, name, autor_name, year, price, isbn):
        self.name = name
        self.autor_name = autor_name
        self.year = year
        self.price = price
        self.isbn = isbn
        
        
        
class InvoiceWithSOLID:
    def __init__(self, book, amount, discount_rate, tax_rate):
        self.book = book
        self.amount = amount
        self.discount_rate = discount_rate
        self.tax_rate = tax_rate
        self.total = self.calculate_total()
        
    def calculate_total(self):
        price = ((self.book.price - self.book.price * self.discount_rate) * self.amount)
        priceWithTax = price * (1 + self.tax_rate)
        return priceWithTax
        