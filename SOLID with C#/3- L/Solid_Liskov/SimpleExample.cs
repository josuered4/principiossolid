public abstract class T
{
    public abstract string GetName();
}

public class S1 : T
{
    public override string GetName()=>"s1";
}

public class S2 : T
{
    public override string GetName()=>"s2";
}