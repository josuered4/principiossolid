//Abstraemos la clase Venta
public abstract class AbstractSale
{
    protected decimal amount;
    protected string? customer;

    public AbstractSale(decimal amount, string customer)
    {
        this.amount = amount;
        this.customer = customer;
    }
    public abstract void Generate();
    
}

public abstract class SaleWithTaxes : AbstractSale
{
    public SaleWithTaxes(decimal amount, string customer, decimal taxes) : base(amount, customer)
    {
        this.taxes = taxes;
    }

    protected decimal taxes;
    public abstract void CalculateTaxes();
}

//Creamos un tipo de venta que hereda de venta 
//Pero que pasa si ahora tienemos que hacer ventas para el extrangero 
//mismo que se realizan de otra manera y pueden tener mas o menos miembros
//o miembros diferentes.
public class LocalSale : SaleWithTaxes //=> al incio hererada de AbstracSale, pero ahora tambien de SaleWithTaxes
{
    public LocalSale(decimal amount, string customer, decimal taxes) : base(amount, customer, taxes)
    {
    }

    public override void CalculateTaxes()
    {
        Console.WriteLine("Se calculan impuestos...");
    }

    public override void Generate()
    {
        Console.WriteLine("Se genera una nueva venta...");
    }
}

//Este es otro tipo de venta, pero al final es una venta que hereda de AbstractSale
public class ForeignSale : AbstractSale
{
    public ForeignSale(decimal amount, string customer) : base(amount, customer)
    {
    }

    public override void Generate()
    {
        Console.WriteLine("Se genera una nueva venta...");
    }
}

//Es principio de trata principalmente sobre los metodos de abstraccion de 
//clases, es un tema complejo que requiere de practica, ver que tienen en comun 
//siertas clases y como se pueden categorizar, pero las clases hijas simpre deben 
//respetar los metodos padres, para que puedar ser usamos como si ellos fueran

public class SaleInvoice : SaleWithTaxes //=> al incio hererada de AbstracSale, pero ahora tambien de SaleWithTaxes
{
    public SaleInvoice(decimal amount, string customer, decimal taxes) : base(amount, customer, taxes)
    {
    }

    public override void CalculateTaxes()
    {
        Console.WriteLine("Se calculan impuestos...");
    }

    public override void Generate()
    {
        Console.WriteLine("Se genera una nueva venta...");
    }
    
    public void XML()=>Console.WriteLine("Generando xml");
}

//Las clases hijas pueden implementar metodos adicionales 
//pero respetando el padre