﻿// See https://aka.ms/new-console-template for more information

class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        //Estamos usando a instancias hijas como padres.
        T s = new S1();
        Console.WriteLine(s.GetName());

        s = new S2();
        Console.WriteLine(s.GetName());


        //Otro Ejemplo.
        // AbstractSale es clase padre y Sale es clase hija de AbstractSale
        AbstractSale sale = new LocalSale(100, "Josue", 0.16m);
        sale.Generate();

        sale = new ForeignSale(100, "Josue"); //Es posible por que ambos son de AbstractSale
        sale.Generate(); 

        //NO ES POSIBLE
        SaleWithTaxes sale2 = new LocalSale(100, "Josue", 0.16m);
        //sale2 = new ForeignSale(100, "Josue"); 
        //aunque sean hijas de AbstractSale, ForeignSale no hereda ambien de SaleWithTaxes
    }
}


