//Tenemos una clase de bajo nivel
//Donde debemos leer un archivo con un json, con un 
//areglo de objetos que se acoplaran al modelo "POST"

using System.Text.Json;

public class InfoByFileSOLID : IInfo
{
    private string _path; //archivo
    public InfoByFileSOLID(string path) { _path =path;}

    public async Task<IEnumerable<Post>> Get()
    {
        var contentStream = new FileStream(_path, FileMode.Open, FileAccess.Read); //Se lee el archivo
        IEnumerable<Post> posts = await JsonSerializer.DeserializeAsync<IEnumerable<Post>>(contentStream); //Se deseraliza el Json
        return posts;
    }
}

//Ahora implementaremos una clase de alto nivel Monitor que ocupa esta clase
