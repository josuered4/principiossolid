//Clase de Alto Nivel
//Clase que requiere del funcionamiento de InforByFile.
//Y TIENE LA FUNCION DE MOSTRAR LA INFORMACION.
//TIENE UN BAJO ACOPLAMIENTO POR QUE NO DEPENDE DE UNA CLASE EN ESPECIFO
//SINO DE UNA INTERFAZ

public class MonitorSOLID
{
    private readonly string _origin; //La direecion del archivo
    private readonly IInfo _info;

    //Se inyecta la dependencia desde el contructor
    public MonitorSOLID(string origin, IInfo info){
        _origin = origin;
        _info = info;
    }

    //Ya no se tiene la dependencia
    public async Task Show()
    {
        var posts = await _info.Get();
        foreach(var post in posts)
            Console.WriteLine(post.title);
    }
}