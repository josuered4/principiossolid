//Funcion de Bajo Nivel pero que tiene una fuerte dependencia a InfoByFile
// Clase cuya funcion es guardar en  un archivo un mensaje 

using System.Text.Json;

public class FileDBSOLID
{
    private string _path;
    private string _origin;
    private IInfo _info;
    public FileDBSOLID(string path, string origin, IInfo info){
        _origin = origin;
        _path = path;
        _info = info;
    }

    public async Task Save(){
        var posts = await _info.Get();
        string json = JsonSerializer.Serialize(posts);
        await File.WriteAllTextAsync(_path, json);
    }
}