﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

//Source 
string originUrl = @"https://jsonplaceholder.typicode.com/todos/";
string origin = @"C:\Users\josue\Desktop\Proyectos\principios SOLID\SOLID with C#\5- D\Solid_Dependencias\posts.json";
string dbPath = @"C:\Users\josue\Desktop\Proyectos\principios SOLID\SOLID with C#\5- D\Solid_Dependencias\db.json";

//Clase Sin inverion de dependencias y fuertemente acopladas.
Monitor monitor = new Monitor(origin); //Minitor instancea a InfoByFil
await monitor.Show();

FileDB file = new FileDB(dbPath, origin);
await file.Save(); //=> mismo caso que con minitor



//Clases Con inversion de dependencias y bajo acoplamiento. 
//En este caso la dependencia es obtracta
IInfo service = new InfoByRequestSOLID(originUrl); //Creamos una instancia del servicio
FileDBSOLID fileDB = new FileDBSOLID(dbPath, origin, service); //Inyectamos la dependencia del servicio
await fileDB.Save();

IInfo service2 = new InfoByFileSOLID(origin); //usamos una clase de mismo tipo pero con diferente servicio
FileDBSOLID fileDB2 = new FileDBSOLID(dbPath, origin, service2); 
await fileDB.Save();



//El usar ese principio
//Hace que al inicio se escriba mas codigo, pero al momento 
//de actuzalizar es el que requiere menos cambios.
