//Clase de bajo nivel igual que InfoByFile 
//pero con peticion a un api
using System.Text.Json;

class InfoByRequest
{
    private string _url;
    public InfoByRequest(string url)
    {
        _url = url;
    }

    public async Task<IEnumerable<Post>> Get()
    {
        HttpClient httpClient = new HttpClient();
        var response = await httpClient.GetAsync(_url);
        var stream = await response.Content.ReadAsStreamAsync();
        IEnumerable<Post> posts = await JsonSerializer.DeserializeAsync<IEnumerable<Post>>(stream);
        return posts; 
    }
}