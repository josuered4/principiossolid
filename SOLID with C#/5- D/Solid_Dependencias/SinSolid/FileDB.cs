//Funcion de Bajo Nivel pero que tiene una fuerte dependencia a InfoByFile
// Clase cuya funcion es guardar en  un archivo un mensaje 

using System.Text.Json;

public class FileDB
{
    private string _path;
    private string _origin;
    public FileDB(string path, string origin){
        _origin = origin;
        _path = path;
    }

    public async Task Save(){
        InfoByFile info = new InfoByFile(_origin); //=> Se tiene la fuerte dependencia 
        //Si quiero cambiar de servicio de un InfoByFile a un InfoByRequest tengo que cambiarlo en todos los lafos
        var posts = await info.Get();
        string json = JsonSerializer.Serialize(posts);
        await File.WriteAllTextAsync(_path, json);
    }
}