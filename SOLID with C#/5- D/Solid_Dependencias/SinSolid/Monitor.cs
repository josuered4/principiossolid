//Clase de Alto Nivel
//Clase que requiere del funcionamiento de InforByFile.
//Y TIENE LA FUNCION DE MOSTRAR LA INFORMACION

public class Monitor
{
    private readonly string _origin; //La direecion del archivo

    public Monitor(string origin){
        _origin = origin;
    }

    public async Task Show()
    {
        InfoByFile info = new InfoByFile(_origin); 
        //=> En esta parte empieza el fuerte a coplamiento o dependencia que debemos cambiar

        var posts = await info.Get();
        foreach(var post in posts)
            Console.WriteLine(post.title);
    }
}