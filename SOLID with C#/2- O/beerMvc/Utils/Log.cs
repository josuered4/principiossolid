namespace beerMvc.Utils;

public class Log
{
    private readonly string _name = "log.txt";

    public async void Save(string content) =>
        await File.WriteAllTextAsync(_name, content);
    
}

//En el proyecto se pidio que se creara una cerveza 
//despues de eso que se guardara y que se enviara a log 
//pero como esta funcion la podemos reutilizar despues 
//lo separaremos en una clase por separado.