using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using beerMvc.Models;
using beerMvc.Service;

namespace beerMvc.Controllers;

public class BeerController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public BeerController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Add(BeerViewModel beer)
    {
        //Tambien podemos dar al controlador la funcion de validar del modelo
        if(!ModelState.IsValid)
            return View(beer);
        
        //Guardado de BBDD
        //Guardado de Log
        var beerService = new BeerService();
        beerService.Create(beer);

        return Ok();
    }
    // De esta manera el controlador solo obtiene la infor, valida y devuelve el 
    // resultado sobre el tratamiento de la información, la demas funcines son delegadas
}
