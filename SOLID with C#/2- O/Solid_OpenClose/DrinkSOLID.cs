//Para mejora la funcion de Invoce debemos de abstraer la case a un interfaz
public interface IDrink
{
    public string Name {get; set;}
    public decimal Price {get; set;}
    public decimal Invoce {get; set;}
    public decimal GetPrice();
}


//Crearemos una clase por cada tipo de bebida 
public class Water : IDrink
{
    public string Name {get; set;}
    public decimal Price {get; set;}
    public decimal Invoce {get; set;}
    public decimal GetPrice() => Price * Invoce;
}

public class Alcohol : IDrink
{
    public string Name {get; set;}
    public decimal Price {get; set;}
    public decimal Invoce {get; set;}
    public decimal Promo {get; set;}
    public decimal GetPrice() => (Price * Invoce)-Promo;
}

public class Sugary : IDrink
{
    public string Name {get; set;}
    public decimal Price {get; set;}
    public decimal Invoce {get; set;}
    public decimal Expiration {get; set;}
    public decimal GetPrice() => (Price * Invoce)-Expiration;
}


class Invoce
{


//Funcion refactorizada 
    //Haremos impementacion de interfaces 
    //De esta forma solo tiene la funcion de obtner el total 
    //e implementando la interfaz podemos extnder nuestra funcionalidad
    //a otro tipos de bebidas que seas de IDrink
    public decimal GetTotal(IEnumerable<IDrink> listDrinks)
    {
        decimal total = 0;
        foreach(var item in listDrinks)
            total += item.GetPrice();

        return total;
    }

    //Ahora si, si queremos agregar una nueva bebida no deberiamos 
    //modificar este elemento.

    //Se debe se pensar en que forma se puede extender, por ejemplo 
    //transformar una cadena de texto en convertirla a mayúsculas o 
    //convertirla a minúsculas, que tienen en comun "Se debe convetir a"
    //podremos crear una I en comun y hacer que ambas clases herreden de esta

}