﻿// See https://aka.ms/new-console-template for more information

class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
    }
}


//Sin principios Solid
//tenemos un conjunto de bebidas
public class Drink
{
    public string Name {get; set;}
    public string Type {get; set;}
    public decimal Price {get; set;}
}

//Tenemos la clase factura con la funcion de obtener la suma cuenta de bebidad.
class InvoceSinSolid
{
    //Funcion Original 
    //Las bebidas tienen impuestos segun su tipo y dependiendo el tipo son diferentes funciones,  
    //en la funcion podemos ver como se implementamos y puede que ya este el requerimiento, pero 
    //que pasa si meten mas productos, modificar el codigo 
    public decimal GetTotalOriginal(IEnumerable<Drink> listDrinks)
    {
        decimal total = 0;
        foreach(var drink in listDrinks)
        {
            switch(drink.Type){
                case "Agua":
                    total += drink.Price;
                    break;
                case "Azucar":
                    total += drink.Price * 3.33m;
                    break;
                case "Alcohol":
                    total += drink.Price * 14.3m;
                    break;
            }
        }
        return total;
    }
}