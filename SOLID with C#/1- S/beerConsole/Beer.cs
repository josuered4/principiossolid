public class Beer
{
    public string Name {get; set;}
    public string Brand  {get; set;}
    public int Alcohol  {get; set;}

    public Beer(String Name, String Brand, int Alcohol){
        this.Name = Name;
        this.Brand = Brand;
        this.Alcohol = Alcohol;
    }
}

public class BeerDB
{
    private Beer _beer;
    public BeerDB(Beer beer){
        _beer = beer;
    }
    public void Save(){
        Console.WriteLine($"Guardamos {_beer.Name} y {_beer.Brand}");
    }
}

public class BeerSend
{
    private Beer _beer;
    public BeerSend(Beer beer){
        _beer = beer;
    }
    public void Send(){
        Console.WriteLine($"Enviamos {_beer.Name} y {_beer.Brand}");
    }
}