public class BeerX
{
    public string Name {get; set;}
    public string Brand  {get; set;}

    public BeerX(String Name, String Brand){
        this.Name = Name;
        this.Brand = Brand;
    }

    public void Save(){
        Console.WriteLine($"Guardamos {Name} y {Brand}");
    }

    public void Send(){
        Console.WriteLine($"Enviamos {Name} y {Brand}");
    }
}