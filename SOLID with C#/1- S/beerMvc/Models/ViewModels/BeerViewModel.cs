//Clase con la responsabilidad de tener la informacion
public class BeerViewModel
{
    public string Name {get; set;}
    public string Brand  {get; set;}
    public int Alcohol  {get; set;}

    public BeerViewModel(String Name, String Brand, int Alcohol){
        this.Name = Name;
        this.Brand = Brand;
        this.Alcohol = Alcohol;
    }
}