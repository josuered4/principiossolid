using beerMvc.Models.DDBB;
using beerMvc.Utils;

namespace beerMvc.Service;

public class BeerService
{
    public void Create(BeerViewModel beer)
    {
        var beerDB = new BeerDB();
        var log = new Log();

        beerDB.Save(beer);
        log.Save($"Se a guardado la cerveza {beer.Name} de la marca {beer.Brand}");
    }
}

//Se hace llamado de las funciones a realizar
//mas no la clase implementa las funciones solo las llama