//Ejemplo 
//Supongamos que creamos una interfaz para poder crear un crud todos sus metodos
//pero, pero que pasa si hay modulos que no implementaran Delete y Update por ejemplo el de ventas.

// public interface ICrud<T>
// {
//     public T Get(int Id);
//     public List<T> GetList();
//     public void Add(T entity);
//     public void Update(T entity);
//     public void Delete(T entity);
// }


//Una solucion seria no implementar los metodos y poner una excepcion, 
//pero eso esta mal por que no deberian implementar esas funciones.

//Aca es donde entra el principio del Segregacion, 
//Tenemos que dividir de forma abstracta, segun el sistema y los requerimientos,
//en este caso dibidemos la interfaz CRUD en dos interfaces, segregando a otras dos interfaces
public interface IBasicAcctions<T>
{
    public T Get(int Id);
    public List<T> GetList();
}

public interface IEditableAcciton<T>
{
    public void Add(T entity);
    public void Update(T entity);
    public void Delete(T entity);
}

//De esta forma los modulos que requieran solo de leer y crear dependeran de una 
//interfaz ajustada a sus requerimientos y si requiere todo el crud, se pueden 
//implementar amabas interfaces.

public class UserService : IBasicAcctions<User>, IEditableAcciton<User>
{
    public User Get(int Id){
        Console.WriteLine("Obtenemos Usuario");
        return new User();
    }
    public List<User> GetList(){
        Console.WriteLine("Lista de Usuarios");
        return new List<User>();
    }
    public void Add(User entity) => Console.WriteLine("Usuario Agregados");
    public void Update(User entity)=> Console.WriteLine("Usuario Actualizado");
    public void Delete(User entity) => Console.WriteLine("Usuario Eliminado");

}

public class SaleService : IBasicAcctions<Sale>
{

    public Sale Get(int Id)
    {
        Console.WriteLine("Obtenemos Ventas");
        return new Sale();
    }

    public List<Sale> GetList()
    {
        Console.WriteLine("Lista de Ventas");
        return new List<Sale>();
    }
}

//El poder dectar que es lo minimo que debe tener una interfaz
//segun los requerimientos, tampoco se debe creear una interfaz por metodo
//Se trata tambien de dectectar la sobra de metodos.

//En la clase List<> Podemos ver un ejemplo mas complejo.