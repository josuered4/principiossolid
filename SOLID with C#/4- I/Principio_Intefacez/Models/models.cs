//Solo son clases modelos.

public class User
{
    public int Id {get; set;}
    public string Name {get; set;}
    public string Email {get; set;}
}

public class Sale
{
    public decimal Amount {get; set;}
    public DateTime Date {get; set;}
}